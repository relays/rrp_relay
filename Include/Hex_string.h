/*******************************************************************************
* File:        Hex_string.h
* Description: Hex_string class for parameter passing in VANET demonstrator
*              It MUST NOT throw
*
* Created:     Saturday 30 June 2018
*
*
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* Practical EMV Distance Bounding: Attacks and Solutions                       *
* ======================================================                       *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification.                                         *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include <cstdint>
#include <string>
#include <vector>

using Byte=unsigned char;

class Hex_string
{
public:
	Hex_string()=default;
	explicit Hex_string(std::string const& str);
	Hex_string(Hex_string const& hs)=default;
	Hex_string(Hex_string&& hs)=default;
	Hex_string& operator=(Hex_string const& hs)=default;
	bool is_valid() const {return valid_;}
	size_t size() const {return hex_string_.size();}
	std::string hex_string() const;
	std::string get_last_error() const;

private:
	bool valid_{false};
	std::string hex_string_;
	std::string error_;
};


