/*******************************************************************************
* File:        Test_apdus.h
* Description: APDUs to use for timing tests
*
*
* Created:     Tuesday 6 October 2020
*
*
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* Practical EMV Distance Bounding: Attacks and Solutions                       *
* ======================================================                       *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification.                                         *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include <vector>
#include <string>

const std::vector<std::string> apdus_rrp{"00a404000e325041592e5359532e444446303100",
				 "00a4040007a000000004101000",
				 "80A800000F830D7A45123EE59C7F40600000300000",
				 "80EA0000041112131400",
				 "80EA0000041112131500",
				 "80EA0000041112131600",
				 "00b2011400",
				 "00b2011c00",
				 "00b2012400",
				 "00b2022c00",
				 "802a8e800401020304"};

constexpr size_t apdus_rrp_n=sizeof(apdus_rrp);
const size_t rrp_index=3;

