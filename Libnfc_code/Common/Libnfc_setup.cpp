/*******************************************************************************
* File:        Libnfc_setup.cpp
* Description: Code for libnfc to setup nfc_devices and cleanly
*			   closedown
*
*
* Created:     Tuesday 1 September 2020
*
*
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* Practical EMV Distance Bounding: Attacks and Solutions                       *
* ======================================================                       *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification.                                         *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#include <exception>
#include <nfc/nfc.h>
#include "Io_utils.h"
#include "Libnfc_setup.h"

Libnfc_devices::Libnfc_devices(bool debug) : debug_(debug), ctx_(nullptr), next_device_(0)
{
	if (debug_) {
		std::cout << "Libnfc_devices ctor\n";
	}
	for (auto& dev : devices_)
	{
		dev=nullptr;
	}

    nfc_init(&ctx_);
    if (ctx_==nullptr)
    {
        throw(std::runtime_error("Unable to setup a libnfc context"));
    }
	if (debug_) {
		std::cout << "nfc_context created\n";
	}
}

size_t Libnfc_devices::add_device(const nfc_connstring dev_string)
{
	if (next_device_==max_device_count) {
		throw(std::runtime_error("No room for extra devices"));
	}
    nfc_device* device=nfc_open(ctx_, dev_string);
    if (device==nullptr) {
        std::string error=vars_to_string("Unable to open device: ", dev_string);
        throw(std::runtime_error(error.c_str()));
    }
	devices_[next_device_]=device;
	return next_device_++;
}

Libnfc_devices::~Libnfc_devices()
{
	if (debug_) {
		std::cout << "Libnfc_devices dtor\n";
	}
	for (auto& dev : devices_)
	{
		if (dev!=nullptr) {
			nfc_close(dev);
			dev=nullptr;
		}
	}
	if (debug_) {
		std::cout << "All devices closed, deleting context\n";
	}
    if (ctx_!=nullptr) {
        nfc_exit(ctx_);
		ctx_=nullptr;
    }
	if (debug_) {
		std::cout << "Context destroyed\n";
	}
}

