/*******************************************************************************
* File:        Libnfc_pn53x_utils.cpp
* Description: Libnfc utilities for the PN53x series of NFC chips
*
*
* Created:     Friday 4 September 2020
*
*
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* Practical EMV Distance Bounding: Attacks and Solutions                       *
* ======================================================                       *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification.                                         *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#include <nfc/nfc.h>
#include "Libnfc_pn53x_utils.h"


std::pair<bool,Byte_buffer> transceive(
nfc_device* nd,
Byte_buffer const& msg,
int timeout,
bool debug
) noexcept
{
    if (debug) {
        std::cout << "transceive: send: " << msg << '\n';
    }
    bool is_ok=true;
    static uint8_t  rx_buffer[PN53x_EXTENDED_FRAME__DATA_MAX_LEN];
    size_t szRx = sizeof(rx_buffer);
    int res = pn53x_transceive(nd, &msg[0], msg.size(), rx_buffer, szRx, timeout);
    if (res <= 0) {
      nfc_perror(nd, "pn53x_transceive: read_register_cmd failed");
      is_ok=false;
      return std::make_pair(is_ok,Byte_buffer{});
    }

    Byte_buffer cmd_response(rx_buffer,res);
    if (debug) {
        std::cout << "transceive: resp: " << cmd_response << '\n';
    }

    return std::make_pair(is_ok,cmd_response);
}
