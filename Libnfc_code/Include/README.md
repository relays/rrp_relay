This include directory **needs** to have a link to the `libnfc` code directory. 

You may use the code directory from the `acr-readers`.

Create a link with (assumes you have not changed the directory structure):

```
ln -sf ../../../timing_data/acr-readers libnfc
```
