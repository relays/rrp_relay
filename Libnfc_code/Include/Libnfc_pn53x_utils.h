/*******************************************************************************
* File:        Pn53x_utils.h
* Description: Utilities for the PN53x series of NFC chips
*

* Created:     Friday 4 September 2020
*

*
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* Practical EMV Distance Bounding: Attacks and Solutions                       *
* ======================================================                       *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification.                                         *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include <nfc/nfc.h>
extern "C"
{ 
  #include "chips/pn53x.h"
}
#include "Byte_buffer.h"

constexpr size_t max_frame_len=263;
constexpr uint8_t sak_ISO14443_4_compliant=0x20;

std::pair<bool,Byte_buffer> transceive(
nfc_device* nd,
Byte_buffer const& msg, 
int timeout,
bool debug
) noexcept;
