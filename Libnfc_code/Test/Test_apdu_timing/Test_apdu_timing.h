/*******************************************************************************
* File:        Test_apdu_timing.h
* Description: Program to measure the timing of transaction APDUs
*
*
* Created:     Saturday 3 October 2020
*
*
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* Practical EMV Distance Bounding: Attacks and Solutions                       *
* ======================================================                       *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification.                                         *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include "Libnfc_setup.h"

#include <map>

enum Option {tests,split_length,help,debug};

const std::map<std::string,Option> program_options{
    {"--tests",tests},
    {"-n",tests},
	{"--split",split_length},
	{"-s",split_length},
    {"--debug", debug},
    {"-g", debug},
    {"--help",help},
    {"-h",help},
};

struct Program_data
{
    size_t n_tests{1};
	size_t apdu_split_length{40};
	int debug_level{0};
};

void usage(std::ostream& os, std::string name);

enum Init_result {init_ok=0,init_failed,init_help};

Init_result initialise(int argc, char *argv[], Program_data& pd);

bool do_test(Libnfc_devices& devices, std::vector<std::string> const& apdus, bool debug, size_t apdu_split_length);
