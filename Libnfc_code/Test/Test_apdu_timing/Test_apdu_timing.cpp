/*******************************************************************************
* File:        Test_apdu_timing.cpp
* Description: Program to mesaure the APDU timing at different heights
*			   and orientations for Mastercard RRP card
*
*
* Created:     Saturday 3 October 2020
*
*
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* Practical EMV Distance Bounding: Attacks and Solutions					   *
* ======================================================                       *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification.                                         *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


/*******************************************************************************
 * 
 *  To help debug use:
 *         LIBNFC_LOG_LEVEL=3 ./test_rrp_timing ...
 * 
**********************************************************************/

#include <iostream>
#include <memory>
#include <thread>
#include <cstdio>
#include <cstdlib>
#include <cstddef>
#include <cstdint>
#include <cmath>
#include <cstring>
#include <signal.h>

#include <nfc/nfc.h>
#include "Byte_buffer.h"
#include "Io_utils.h"
#include "Clock_utils.h"
#include "Libnfc_setup.h"
#include "Libnfc_pn53x_utils.h"
#include "Card_utils.h"
#include "Test_apdus.h"
#include "Test_apdu_timing.h"

static void intr_hdlr(int sig)
{
	(void) sig;
	std::cout << "\nQuitting...\n";
	exit(EXIT_FAILURE);
}

void usage(std::ostream& os, const char* name)
{
 	os << "Usage: " << name << "\n\t-h, --help - this message\n"
	 				<< "\t-n, --tests <number of tests>        [1]\n"
					<< "\t-s, --split <apdu split length>      [40]\n"
                    << "\t-g, --debug <debug level> (0,1,2)    [0]\n";
}

int main(int argc, char *argv[])
{
	Program_data pd;
	Init_result ir=initialise(argc,argv,pd);
	if (ir!=Init_result::init_ok) {
		return (ir==Init_result::init_help)?EXIT_SUCCESS:EXIT_FAILURE;
	}
	
	size_t n_tests=pd.n_tests;
	bool tests_ok{true};
	int debug=pd.debug_level;

	size_t apdu_split_length=pd.apdu_split_length;

    signal(SIGINT, intr_hdlr);
	std::cout << std::setprecision(6);
	try {
		Libnfc_devices devices(debug);
		nfc_connstring connstrings[max_device_count];
		size_t n_found = nfc_list_devices(devices.nfc_ctx(),connstrings,max_device_count);
		for (size_t i=0;i<n_found;++i)
		{
			devices.add_device(connstrings[i]);
			if (debug) {
				std::cout << "Libnfc device created for " << connstrings[i] << '\n';
			}
		}

		std::vector<std::string> const &apdus=apdus_rrp;
		std::vector<float> timings;
		for (size_t t=0;t<n_tests;++t) {
			tests_ok=do_test(devices,apdus,debug,apdu_split_length);
		}
	}
	catch (std::exception& e) {
		std::cerr << e.what() << '\n';
		return EXIT_FAILURE;
	}

	return tests_ok?EXIT_SUCCESS:EXIT_FAILURE;
}

Init_result initialise(int argc, char *argv[], Program_data& pd)
{
	if (argc==1) {
		return Init_result::init_ok;
	}

    int arg=1;
    while (arg<argc)
    {
        auto search=program_options.find(argv[arg++]);
        if (search==program_options.end())
        {
            std::cerr << "Invalid option: " << argv[arg-1] << '\n';
            usage(std::cerr,argv[0]);
            return Init_result::init_failed;
        }
        Option o=search->second;
        switch (o)
        {
        case Option::tests:
            pd.n_tests=std::atoi(argv[arg++]);
            break;
        case Option::split_length:
			pd.apdu_split_length=std::atoi(argv[arg++]);
			break;
        case Option::help:
            usage(std::cout,argv[0]);
            return Init_result::init_help;
        case Option::debug:
            {
                std::string level(argv[arg]);
                if (level!="0" && level!="1" && level!="2")
                {
                    usage(std::cerr,argv[0]);
                    std::cerr << "Debug levels are 0, 1 or 2 (default 0)\n";
                    return Init_result::init_failed;            
                }
                pd.debug_level=atoi(argv[arg++]);
            }
            break;
        default:
            std::cerr << "Invalid option: " << argv[arg-1] << '\n';
            usage(std::cerr,argv[0]);
            return Init_result::init_failed;
        }
    }

	return Init_result::init_ok;
}

bool do_test(Libnfc_devices& devices, std::vector<std::string> const& apdus, bool debug, size_t apdu_split_length)
{
	bool fail{false};
	nfc_device* dummy_reader_ptr=devices[0];
    if (nfc_initiator_init(dummy_reader_ptr) < 0) {
        std::cerr << "Error: failed initializing the reader\n";
        return fail;
    }

    // Try to find a ISO 14443-4A tag
	if (debug) {
		std::cout << "Put a card on the reader device\n";
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
	nfc_target real_tag;
    nfc_modulation nm;
    nm.nmt = NMT_ISO14443A;
    nm.nbr = NBR_106;
    if (nfc_initiator_select_passive_target(dummy_reader_ptr, nm, NULL, 0, &real_tag) <= 0) {
		std::cerr << "Error: no card detected\n";
        return fail;
	}

	if (debug) {
		std::cout << "Found tag:\n";
		print_iso14443A_tag_info(std::cout,real_tag);
	}
	uint8_t card_reply[max_frame_len];
	size_t card_reply_len;
	int res=0;

	F_timer_mu timer;
	if (debug) {
		std::cout << std::defaultfloat << std::fixed << std::setprecision(4);
	}
	size_t n_apdus=apdus.size();
	for (int i=0;i<n_apdus;++i)
	{
		Byte_buffer apdu(Hex_string(apdus[i]));
		if (debug) {
			std::cout << floor(timer.get_duration()+0.5)/1000 << "\tC-APDU: ";
			print_apdu(std::cout,apdu,apdu_split_length,"\t        ");
			std::cout << '\n'; 
		}

		if ((res = nfc_initiator_transceive_bytes(dummy_reader_ptr, apdu.cdata(), apdu.size(), card_reply, sizeof(card_reply), -1)) < 0) {
			std::cerr << "nfc_initiator_transceive_bytes failed, returned: " << res <<'\n';
			return false;
		} else {
			card_reply_len=static_cast<size_t>(res);
		}

		std::cout << floor(timer.get_duration()+0.5)/1000 << "\tR-APDU: ";
		Byte_buffer card_reply_bb(card_reply,card_reply_len);
		print_apdu(std::cout,card_reply_bb,apdu_split_length,"\t        ");
		std::cout << std::endl;
	}

    Byte_buffer get_fw_version{GetFirmwareVersion};
	if (debug) {
		std::cout << "Get firmware version (resets PN53x): ";
	}
	auto rx_data=transceive(dummy_reader_ptr,get_fw_version,0,debug);
    if (debug && rx_data.first) {
    	std::cout << rx_data.second << '\n';
    }

    return true;
}
