# ===========================
#  Makefile for NFC code
# ===========================

$(info Compiling with $(CXX))

all:
	make -s -C ./Libnfc_code/Test

clean:
	@make clean -s -C ./Libnfc_code/Test
