/*******************************************************************************
* File:        Card_utils.cpp
* Description: Utilities for the contactless cards
*
*
* Created:     Monday 5 October 2020
*
*
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* Practical EMV Distance Bounding: Attacks and Solutions                       *
* ======================================================                       *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification.                                         *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#include <iostream>
#include <nfc/nfc.h>
#include "Io_utils.h"
#include "Byte_buffer.h"

void print_apdu(std::ostream& os, Byte_buffer const& apdu, size_t split_length, std::string const& extra_line_indent)
{
	size_t apdu_length=apdu.size();
	if (apdu_length<=split_length) {
		os << apdu;
		return;
	}

	size_t apdu_offset=split_length;
	os << apdu.get_part(0,split_length);
	while (apdu_length-apdu_offset>split_length) {
		os << '\n' << extra_line_indent << apdu.get_part(apdu_offset,split_length);
		apdu_offset+=split_length;
	}
	if (apdu_length-apdu_offset>0)
	{
		os << '\n' << extra_line_indent << apdu.get_part(apdu_offset,apdu_length-apdu_offset);
	}
	return;
}

void print_iso14443A_tag_info(std::ostream& os, const nfc_target& tag)
{
	if (tag.nm.nmt!=NMT_ISO14443A) {
		std::cerr << "print_iso14443A_tag: incorrect tag type\n";
	}
	nfc_iso14443a_info const& iso_info=tag.nti.nai;
	os << "ATQA: ";
	print_hex_byte(os,iso_info.abtAtqa[1]);
	print_hex_byte(os,iso_info.abtAtqa[0]);
	os << "\n SAK: ";
	print_hex_byte(os,iso_info.btSak);
	os << "\n UID: ";
	print_buffer(os,iso_info.abtUid,iso_info.szUidLen,false);		
	os << "\n ATS: ";
	print_buffer(os,iso_info.abtAts,iso_info.szAtsLen,false);
	os << std::endl;		
}
