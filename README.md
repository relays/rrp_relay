The code in this folder implements the replay of APDUs to a Mastercard RRP test
card.

The code uses libnfc which needs to be installed. There should be a link to the
libnfc include directory in the Libnfc_code/Include directory.

The code compiles with gcc version 7.5.0, program options are: 

-h, --help - this message\n"
-n, --tests <number of tests>        [1]
-s, --split <apdu split length>      [40]
-g, --debug <debug level> (0,1,2)    [0]



